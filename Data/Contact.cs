using Microsoft.WindowsAzure.Storage.Table;

namespace regweb
{
    public class Contact : TableEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public bool NotifyViaEmail { get; set; }
        public string Phone { get; set; }
        public bool NotifyViaText { get; set; }
        public int NumberAttending { get; set; }
        public int NumberOfChildrenAttending { get; set; }
    }

}