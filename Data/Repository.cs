using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace regweb
{
    public interface Repository
    {
        Task Save(Contact contact);
        Task<List<Contact>> GetAll();
    }

    public class RepositoryImpl : Repository
    {
        private readonly CloudTable _table;

        public RepositoryImpl()
        {
            var acct = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=ocsdatastorage;AccountKey=gO5a5NPDi6Saf7mI6osPfXwlHHAVTQQh1ynK8KPm5U1Zlc9HY3uOp46W5nMq6VCOYE2XAXXGcJ2tbM9WbG3TBA==;EndpointSuffix=core.windows.net");
            var tc = acct.CreateCloudTableClient();
            _table = tc.GetTableReference("dbName");
        }

        public async Task Save(Contact contact)
        {
            await _table.ExecuteAsync(TableOperation.InsertOrReplace(contact));
        }

        public async Task<List<Contact>> GetAll()
        {
            TableContinuationToken token = null;
            var contacts = new List<Contact>();
            var query = new TableQuery<Contact>();
            do
            {
                TableQuerySegment<Contact> results = await _table.ExecuteQuerySegmentedAsync(query, token);
                token = results.ContinuationToken;
                contacts.AddRange(results.Results);
            } while (token != null);
            return contacts;
        }
    }
}