using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Configuration;
using CsvHelper;
using System.Text;
using System.Linq;

namespace regweb
{
    public interface MailQueue
    {
        Task Send(Contact contact);
        Task Send(List<Contact> contacts);
    }

    public class MailQueueImpl : MailQueue
    {
        private readonly RenderView _renderer;
        private List<string> _sendTo;

        public MailQueueImpl(RenderView renderer, IConfiguration config)
        {
            _renderer = renderer;
            _sendTo = config.GetValue<string>("email_distribution_list", "doug.l.mitchell@gmail.com").Split(",").ToList();
        }

        private CloudQueue _queue;
        private async Task<CloudQueue> getQueue()
        {
            if (_queue == null)
                _queue = await QueueFactory();

            return _queue;
        }

        private async Task<CloudQueue> QueueFactory()
        {
            var acct = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=ocsstore;AccountKey=8wrNq1ozGulfM0R6dC97pFyVLM/8mGFLPlT7E5f4h1cCcnzHCBclQ8Vct9+teDR4M/U008kLvDWny9a90EfR7Q==;EndpointSuffix=core.windows.net");
            var client = acct.CreateCloudQueueClient();
            var queue = client.GetQueueReference("ocs-email");
            await queue.CreateIfNotExistsAsync();
            return queue;
        }

        public async Task Send(Contact contact)
        {
            var res = await _renderer.RenderToStringAsync("Email/InterestNotification", contact);
            var mail = new OcsMail
            {
                HtmlBody = res,
                To = _sendTo,
                From = "fishnet@onlinechristianservices.org",
                Subject = "Event Registration"
            };

            var json = JsonConvert.SerializeObject(mail);
            var q = await getQueue();
            await q.AddMessageAsync(new CloudQueueMessage(json));
        }

        public async Task Send(List<Contact> contacts)
        {
            // convert to csv
            var sw = new StringWriter();
            var writer = new CsvWriter(sw);
            writer.WriteRecords(contacts);

            var res = await _renderer.RenderToStringAsync("Email/AllRegistrations", null);
            var mail = new OcsMail
            {
                HtmlBody = res,
                To = _sendTo,
                From = "fishnet@onlinechristianservices.org",
                Subject = "Event Registrations",
                Attachments = new List<MailAttachment>{
                    new MailAttachment {
                        Type = "text/csv",
                        FileName = $"{DateTime.UtcNow.ToString("yyyy_MM_dd")}_interests_complete.csv",
                        Data = sw.ToString()
                    }
                }
            };

            var json = JsonConvert.SerializeObject(mail);
            var q = await getQueue();
            await q.AddMessageAsync(new CloudQueueMessage(json));
        }
    }

    public class MailAttachment
    {
        public string Type { get; set; }
        public string FileName { get; set; }
        public string Data { get; set; }
    }

    public class OcsMail
    {
        public string HtmlBody { get; set; }
        public string From { get; set; }
        public List<string> Bcc { get; set; }
        public List<string> To { get; set; }
        public string Subject { get; set; }
        public List<MailAttachment> Attachments { get; set; }
        public void AddTo(string to)
        {
            if (To == null)
                To = new List<string>();

            To.Add(to);
        }

        public void AddBcc(string to)
        {
            if (Bcc == null)
                Bcc = new List<string>();

            Bcc.Add(to);
        }
    }
}
